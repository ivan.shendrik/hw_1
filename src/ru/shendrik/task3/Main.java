package ru.shendrik.task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //объявляем сканер
        Scanner scanner = new Scanner(System.in);

        //объявляем значения
        System.out.print("По оси Y: ");
        int x = scanner.nextInt();
        System.out.print("По оси X: ");
        int y = scanner.nextInt();

        //Строим матрицу
        for (int i = 1; i < x + 1; i++) {
            for (int k = 1; k < y + 1; k++) {
                System.out.print(k * i + "  ");
            }
            //Перевод коретки
            System.out.println("");
        }
    }
}
