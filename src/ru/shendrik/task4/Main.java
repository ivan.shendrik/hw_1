package ru.shendrik.task4;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //объявляем сканер
        Scanner scanner = new Scanner(System.in);

        //объявляем значения
        System.out.print("Число:  ");
        int n = scanner.nextInt();
        int ret = 1;

        //Производим расчет
        for (int i = 1; i <= n; i++) {
            ret = ret * i;
        }
        //Выводим результ:
        System.out.println(ret);
    }
}
