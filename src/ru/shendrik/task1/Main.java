package ru.shendrik.task1;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //объявляем сканер
        Scanner scanner = new Scanner(System.in);

        //объявляем значения
        System.out.print("Стоимость за один литр: ");
        int price = scanner.nextInt();
        System.out.print("Количиство литров: ");
        int volume = scanner.nextInt();

        //Выводим результ:
        System.out.print("Всего:" + (price * volume) + " Рублей");

    }
}

