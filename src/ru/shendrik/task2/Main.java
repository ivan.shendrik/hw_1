package ru.shendrik.task2;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //объявляем сканер
        Scanner scanner = new Scanner(System.in);

        //объявляем значения
        System.out.print("Введите первое значение: ");
        int a = scanner.nextInt();

        System.out.print("Введите второе значение: ");
        int b = scanner.nextInt();

        System.out.print("Введите третье значение: ");
        int c = scanner.nextInt();

        System.out.print("Введите четвертое значение: ");
        int d = scanner.nextInt();

        //Выводим результ:
        System.out.println(Math.min(a, Math.min(b, Math.min(c,d))));

    }
}
